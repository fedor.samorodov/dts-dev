/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'dts',
    environment: environment,
    firebase: 'https://dtsdashboard.firebaseio.com/',
    baseURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'hash';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  ENV.contentSecurityPolicy = {
    'default-src': "self",
    'script-src': "'self' 'unsafe-inline' 'unsafe-eval'", 
    'font-src': "'self'", 
    'connect-src': "'self' http://198.211.127.110/ https://auth.firebase.com wss://*.firebaseio.com", 
    'img-src': "'self' https://secure.gravatar.com/",
    'style-src': "'self' 'unsafe-inline' 'unsafe-eval'", 
    'media-src': "'self'"
  };

  if (environment === 'production') {

  }

  return ENV;
};
