import Ember from 'ember';

export default Ember.Controller.extend({
	actions : {
		login : function  () {
			var _this = this;
			var ref = new Firebase("https://dtsdashboard.firebaseio.com");
			ref.authWithPassword({
			  email    : _this.get('email'),
			  password : _this.get('password')
			}, function(error, authData) {
			  if (error) {
			    alert('Error');
			  } else {
			    _this.store.query('user', { orderBy: 'uid', equalTo: authData.uid }).then(function(user) {
					var curentUser = user.get('firstObject');
					if (curentUser.get('projects.length') == 1) {
						_this.transitionToRoute('dashboard', curentUser.get('projects.firstObject'));
					} else {			   		
						_this.transitionToRoute('projects');
					}
				});
						  
			  }
			});
		}
	}
});
