import Ember from 'ember';

export default Ember.Controller.extend({
	actions: {
		changeManager: function  (project, manager) {
			$('#managers').modal('show');
			this.set('currentProject', project);
		},

		addDev: function  (project) {
			$('#developers').modal('show');
			this.set('currentProject', project);
		},

		addManager: function(manager) {
			var project = this.get('currentProject');
			this.store.find('user', project.get('manager.id')).then(function(user) {
				user.get('projects').removeObject(project);
				user.save();
			});
			project.set('manager', manager);
			manager.get('projects').pushObject(project);
			project.save().then(function() {
				$('#managers').modal('hide');
				manager.save();
			})
		},

		addDevel: function(developer) {
			var project = this.get('currentProject')
			project.get('developers').pushObject(developer);
			developer.get('projects').pushObject(project);
			project.save().then(function() {
				$('#developers').modal('hide');
			}).then(function(){
				developer.save();
			})
		},
		deleteDev: function(dev, prod) {
			dev.get('projects').removeObject(prod);
			prod.get('developers').removeObject(dev).then(function() {
				prod.save();
			}).then(function() {
				dev.save();
			})
		},
        logOut: function() {
            var ref = new Firebase("https://dtsdashboard.firebaseio.com");
            ref.unauth();
            this.transitionToRoute('login');
        }
	}
});
