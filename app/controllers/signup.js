import Ember from 'ember';

export default Ember.Controller.extend({
    actions: {
        signUp: function() {
            var _this = this;
            var role = this.get('role') || 3;
            var ref = new Firebase("https://dtsdashboard.firebaseio.com");
            ref.createUser({
                email: _this.get('email'),
                password: _this.get('password')
            }, function(error, userData) {
                if (error) {
                    console.log("Error creating user:", error);
                } else {
                    ref.authWithPassword({
                        email: _this.get('email'),
                        password: _this.get('password')
                    }, function(error, authData) {
                        if (error) {
                            alert('Auchh')
                        } else {
                            var user = _this.store.createRecord('user', {
                                uid: userData.uid,
                                mail: _this.get('email'),
                                name: _this.get('name'),
                                company: _this.get('company'),
                                role: role,
                                phone: _this.get('phone')
                            });
                            if (role == 3) {
                                _this.store.query('user', {
                                    orderBy: 'mail',
                                    equalTo: 'nikita@devteam.space'
                                }).then(function(users) {
                                    var curentUser = users.get('firstObject');
                                    var userName = user.get('name').split(' ')[0];
                                    var project = _this.store.createRecord('project', {
                                        owner: user,
                                        step: '0',
                                        date: new Date(),
                                        manager: users.get('firstObject'),
                                        status: '0',
                                        deadline: 'N/A'
                                    });
                                    var report = _this.store.createRecord('report', {
                                        owner: curentUser,
                                        date: new Date(),
                                        body: 'Hi '+ userName + '!<br/>Welcome to the DevTeamSpace. We are ready to knock it out of the park for you!<br><br/>Please describe your project and add specifications below. I will be in touch with you shortly.'  
                                    });
                                    project.get('reports').pushObject(report);
                                    report.save().then(function() {
                                        user.get('projects').pushObject(project);
                                    }).then(function() {
                                        project.save();
                                    }).then(function() {
                                        _this.set('email', '');
                                        _this.set('name', '');
                                        _this.set('company', '');
                                        _this.set('role', '');
                                        _this.set('phone', '');
                                        user.set('image', authData.password.profileImageURL);
                                        user.save().then(function() {
                                            if (user.get('projects.length') <= 1 && user.get('role') == 3) {
                                                _this.transitionToRoute('dashboard', user.get('projects.firstObject'))
                                            } else {                    
                                                _this.transitionToRoute('projects');
                                            }
                                        })
                                    });
                                

                                });
                            } else {
                                _this.set('email', '');
                                _this.set('name', '');
                                _this.set('company', '');
                                _this.set('role', '');
                                _this.set('phone', '');
                                user.set('image', authData.password.profileImageURL);
                                user.save().then(function() {
                                    if (user.get('projects.length') <= 1 && user.get('role') == 3) {
                                        _this.transitionToRoute('dashboard', user.get('projects.firstObject'))
                                    } else {                    
                                        _this.transitionToRoute('projects');
                                    }
                                })
                            }
                            
                        }
                    });
                }
            });
        }
    }
});