import Ember from 'ember';

export default Ember.Controller.extend({

    actions: {
        save: function() {
            var _this = this;
            this.set('project.isEdit', false);
            this.get('user').save().then(function() {
                    _this.get('project').save();
            })  
            if (this.get('project.step') == 0) {
                this.set('project.step', '1');
                this.get('project').save();
            } 
               
        },
        setStatus: function() {
            var _this = this.get('project');
            if (this.get('user.role') == 1 || this.get('user.role') == 4) {
                $(".progress-bar").click(function(e) {
                    var width = $(this).width();
                    var offset = $(this).offset();
                    var relativeX = (e.pageX - offset.left);
                    var k = relativeX*100/width;
                    _this.set('status', k.toFixed());
                    _this.save();
                });
            }
        },
        editReport: function(report) {
            report.set('isEdit', true);
        },
        saveReport: function(report) {
            report.set('isEdit', false);
            report.save();
        },
        newRepotr: function() {
            var project = this.get('project')
            var report = this.store.createRecord('report', {
                owner: this.get('user'),
                date: new Date(),
                body: this.get('report')
            });
            project.get('reports').pushObject(report);
            report.save().then(function() {
                project.save();
            });
            $.ajax({
                    type: "POST",
                    url: "http://198.211.127.110/api/v1/sendMessage",
                    data: { 
                        to: this.get('project.owner.mail'), 
                        subject: 'New report to ' + this.get('project.title') + ', from ' + this.get('user.name') + ', DevTeam.Space',
                        body: "Hello, " + this.get('project.owner.firstName') + "%0A%0ANew report with following content was added to the project '" +  this.get('project.title') + "':%0A%0A" + this.get('report') + "%0A%0ADevteam Space"
                    },
                    contentType: "application/x-www-form-urlencoded"
            });
            $('#report').modal('hide');
        },
        addLink: function() {
            var project = this.get('project')
            if (project.get('step') == 1) {
                project.set('step', 2);
            }
            
            var link = this.store.createRecord('link', {
                title: this.get('linkTitle'),
                link: this.get('linkHref')
            });
            project.get('links').pushObject(link);
            link.save().then(function() {
                project.save();
            })    
            $('#link').modal('hide');
        },


        addDate: function() {
            this.set('isEstimated', true);
        },

        addBlock: function() {
            this.set('isRoad', true);
        },

        saveRoadblocks: function() {
            this.set('isRoad', false);
            this.get('project').save();
        },
        addTask: function() {
            this.set('isTask', true);
        },

        saveTask: function() {
            this.set('isTask', false);
            this.get('project').save();
        },
    /*    addTask: function() {
            var task = this.store.createRecord('task', {
                name: this.get('tasks')
            });
            this.get('project.tasks').pushObject(task);
            task.save();
            this.get('project').save();
            this.set('tasks', '');
        },

        deleteTask: function(list) {
            if (window.confirm("Элемент будет удален безвозвратно, продолжить?")) {
                var task = this.get('project');
                list.destroyRecord().then(function() {
                    project.save();
                });
            }
        },

*/
        deleteLink: function(list) {
                var project = this.get('project');
                list.destroyRecord().then(function() {
                    project.save();
                });
        },
        compliteTask: function(list) {
            list.set('isComplite', 'complite');
            list.save();
        },

        saveDate: function() {
            this.set('isEstimated', false);
            this.get('project').save();
        },

        cancel: function() {
            this.set('project.isEdit', false);
            this.get('project').rollbackAttributes();
            this.get('user').rollbackAttributes();
        },

        edit: function() {
            this.set('project.isEdit', true);
        },

        startProject: function() {
            var project = this.get('project');
            project.set('step', '3');
            var report = this.store.createRecord('report', {
                owner: this.get('project.manager'),
                isReport: true,
                date: new Date(),
                body: this.get('project.owner.firstName') + ', congrats on starting the project!<br/>I will take care of everything and will contact you soon.'
            });
            
            project.get('reports').pushObject(report);
            report.save().then(function() {
                project.save();
            })          
        },

        logOut: function() {
            var ref = new Firebase("https://dtsdashboard.firebaseio.com");
            ref.unauth();
            this.transitionToRoute('login');
        }
    }
});