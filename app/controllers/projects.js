import Ember from 'ember';

export default Ember.Controller.extend({
    actions: {
        newProject: function() {
        	var _this = this
            var user = this.get('user'); 
            _this.store.query('user', {
                orderBy: 'mail',
                 equalTo: 'nikita@devteam.space'
            }).then(function(users) {
                var curentUser = users.get('firstObject');
                var userName = user.get('name').split(' ')[0];
                var project = _this.store.createRecord('project', {
                    owner: user,
                    step: '0',
                    date: new Date(),
                    manager: curentUser,
                    status: '0',
                    deadline: 'N/A',
                });
                var report = _this.store.createRecord('report', {
                    owner: curentUser,
                    date: new Date(),
                    body: 'Hi '+ userName + '!<br/>Welcome to the DevTeamSpace. We are ready to knock it out of the park for you!<br><br/>Please describe your project and add specifications below. I will be in touch with you shortly.'  
                });
                user.get('projects').pushObject(project);
                project.get('reports').pushObject(report);
                project.save().then(function() {
                    user.save();
                }).then(function() {
                    report.save();
                    _this.transitionToRoute('dashboard', project);
                })
            });
        },

        logOut: function() {
            var ref = new Firebase("https://dtsdashboard.firebaseio.com");
            ref.unauth();
            this.transitionToRoute('login');
        }
    }
});