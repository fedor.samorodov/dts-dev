import Ember from 'ember';

export default Ember.Controller.extend({
	actions: {
		logOut: function  () {
			var ref = new Firebase("https://dtsdashboard.firebaseio.com");
			ref.unauth();
			this.transitionToRoute('login');
		}
	}
});
