import Ember from 'ember';

export default Ember.Component.extend({
	  didInsertElement: function() {
	    jQuery(".jsLeft").width(jQuery("#l-col-1").width() + "px"); 
		jQuery(".customscroll").height(jQuery(window).height() - 100 + "px"); 

		jQuery(".hiddenArea, .hiddenAreaIn").height(jQuery(window).height() - 120 + "px"); 
	    jQuery(".jsRight").width(jQuery("#l-col-2").width() + "px");  
	    jQuery(".header").width(jQuery(".container").width() + "px");
	    jQuery("#content").show();
	    setTimeout(function() {
	    	$('.allreports').each(function() {
	    	if($(this).attr('data-role') == 2 && ($(this).attr('data-id') != $(this).attr('data-user')) ) {
	    		$(this).find('.editRepo').remove();
	    	} else if($(this).attr('data-role') == 3) {
	    		$(this).find('.editRepo').remove();
	    	}
	    });
	    }, 500);   
	  }
});