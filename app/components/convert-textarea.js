import Ember from 'ember';

export default Ember.Component.extend({
	didInsertElement: function(){
		setTimeout(function() {
	        Ember.$('.converter').each(function() {
	        	var txet = Ember.$(this).attr('data-text');
	        	Ember.$(this).html(txet);
	        });
	    }, 700);       
    }
});
