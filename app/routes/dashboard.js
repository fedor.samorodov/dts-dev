import Ember from 'ember';

export default Ember.Route.extend({

	model: function  (params) {
	    return this.store.find('project', params.project_id)
	},

	setupController: function  (controller, model) {


		controller.set('project', model);

		var ref = new Firebase("https://dtsdashboard.firebaseio.com");
		var authData = ref.getAuth();

		if (authData) {
		  this.store.query('user', { orderBy: 'uid', equalTo: authData.uid }).then(function(users) {
		      controller.set('user', users.get('firstObject'));
		  });

		} else {
		  this.transitionTo('login')
		}		
	} 


});
