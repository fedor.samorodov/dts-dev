import Ember from 'ember';

export default Ember.Route.extend({

	setupController: function  (controller, model) {

		var ref = new Firebase("https://dtsdashboard.firebaseio.com");
		var authData = ref.getAuth();
		var _this = this;

		if (authData) {
		  this.store.query('user', { orderBy: 'role', equalTo: 3}).then(function(users1) {
		      controller.set('customers', users1);
		  });
		  this.store.query('user', { orderBy: 'uid', equalTo: authData.uid }).then(function(user) {
			    var curentUser = user.get('firstObject')
			    if (!curentUser.get('isSuperAdmin')) {
			    	_this.transitionTo('projects');
			    }
		  });
		  this.store.query('user', { orderBy: 'role', equalTo: 1 }).then(function(users2) {
		      controller.set('managers', users2);
		  });
		  this.store.query('user', { orderBy: 'role', equalTo: 2 }).then(function(users3) {
		      controller.set('developers', users3);
		  });

		} else {
		  this.transitionTo('login')
		}		
	} 


});
