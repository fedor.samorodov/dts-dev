import Ember from 'ember';

export default Ember.Route.extend({
	setupController: function  (controller, model) {
		var ref = new Firebase("https://dtsdashboard.firebaseio.com");
		var authData = ref.getAuth();
		controller.set('authData', authData)
		this.transitionTo('projects')	
	}
});
