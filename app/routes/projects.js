import Ember from 'ember';

export default Ember.Route.extend({

	setupController: function  (controller, model) {
		var ref = new Firebase("https://dtsdashboard.firebaseio.com");
		var authData = ref.getAuth();
		var _this = this
		if (authData) {
		    this.store.query('user', { orderBy: 'uid', equalTo: authData.uid }).then(function(user) {
			    var curentUser = user.get('firstObject');
			    controller.set('user', curentUser);
		  });

		} else {
		  this.transitionTo('login')
		}		
	}
});
