import DS from 'ember-data';

export default DS.Model.extend({
  uid: DS.attr('string'),
  image: DS.attr('string'),
  projects: DS.hasMany('project', { inverse: null }),
  mail: DS.attr('string'),
  name: DS.attr('string'),
  company: DS.attr('string'),
  role: DS.attr('number'),
  мanaged: DS.attr('number'),
  сompleted: DS.attr('number'),
  phone:  DS.attr('string'),
  isSuperAdmin: function() {
    return this.get('role') == 4 ? true : false;
  }.property('role'),
  isCustomer: function() {
    return this.get('role') == 3 ? true : false;
  }.property('role'),
  isManager: function() {
    return this.get('role') == 1 ? true : false;
  }.property('role'),
  isDeveloper: function() {
    return this.get('role') == 2 ? true : false;
  }.property('role'),
  length: function() {
    return this.get('projects.length');
  }.property('projects.length'),
  sortedProjects: function () {
    var items = this.get('projects').toArray();
    return items.sort(function (lhs, rhs) {
      return rhs.get('date') - lhs.get('date');
    });
  }.property('projects.@each.date'),
  firstName: function() {
    return this.get('name').split(' ')[0];
  }.property('user.name')
});
