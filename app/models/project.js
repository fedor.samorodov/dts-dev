import DS from 'ember-data';

export default DS.Model.extend({
  owner: DS.belongsTo('user', { inverse: null}),
  title: DS.attr('string'),
  description: DS.attr('string'),
  manager:  DS.belongsTo('user', { inverse: null }),
  developers: DS.hasMany('user', { inverse: null}),
  reports: DS.hasMany('report'),
  links: DS.hasMany('link'),
  status: DS.attr('string'),
  deadline: DS.attr('string'),
  date: DS.attr('date'),
  roadblocks: DS.attr('string'),
  step: DS.attr('number'),
  tasks: DS.attr('string'),

  sortedItems: function () {
    var items = this.get('reports').toArray();
    return items.sort(function (lhs, rhs) {
      return rhs.get('date') - lhs.get('date');
    });
  }.property('reports.@each.date'),

  stratProjectBut: function() {
    if (this.get('step') != 3 && this.get('step') != 0 ) {
      return true
    } else {
      return false
    }
  }.property('step'),

  isNew: function() {
    return (this.get('step') == 0 && !this.get('title')) ? true : false
  }.property('title'),

  isEdit: function() {
    return this.get('step') == 0 ? true : false
  }.property('step'),

  isLinks: function() {
    return this.get('step') == 1 ? true : false
  }.property('step'),

  isReady: function() {
    return this.get('step') == 2 ? true : false
  }.property('step'),

  isEmptyDevelopers: function() {
    return this.get('developers.length') == 0 ? true : false
  }.property('developers'),

  statusField: function() {
    if (this.get('stratProjectBut')) {
      return this.get('status') + '%, Not Started'
    } else if (this.get('status') == 100) {
      return this.get('status') + '%, Finished'
    } else {
      return this.get('status') + '%, In progress' 
    }
  }.property('status'),

  blockers: function() {
    return this.get('roadblocks') ? 'warning' : 'btn-default'
  }.property('roadblocks')

});
