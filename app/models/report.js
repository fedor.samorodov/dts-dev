import DS from 'ember-data';
import moment from 'moment';
export default DS.Model.extend({
  body: DS.attr('string'),
  isReport: DS.attr('boolean'),
  date: DS.attr('date'),
  owner: DS.belongsTo('user'),
  formattedDate: function() {
    var date = this.get('date');
    return moment(date).format('LL');
  }.property('date')
});
