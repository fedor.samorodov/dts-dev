import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('projects');
  this.route('dashboard', { path: "dashboard/:project_id" });
  this.route('login');
  this.route('signup');
  this.route('administrate');
});

export default Router;
